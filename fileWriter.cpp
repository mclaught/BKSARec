#include <iostream>
#include <sys/time.h>
#include <time.h>
#include <iomanip>

#include "fileWriter.h"
#include "wiringPi.h"

using namespace std;

std::mutex fw_mutex;

FileWriter::FileWriter(const std::string &pathToWav, const std::string &pathToTime, const std::string pathToLog){
	folderPath = pathToWav;
	timeFilePath = pathToTime;
        logPath = pathToLog;
        failure = 0;
        
        pinMode(1,OUTPUT);

	for (auto &row : ioMatrix){
		for (auto &value : row){
			value = false;
		};
	};
}

void FileWriter::openFile(int in, int out)
{
    auto currentTime = getCurrentTime();
    deleteOldFiles();
    std::string folderPath = getWavFileFolder (currentTime, in, out);
    std::string fileName = getWavFileName(currentTime, in, out);
    saveCurrentTime();
    createFolderRecursive(folderPath);
    fileMatrix [in][out].open( fileName, folderPath, WavFile::WAV_16BIT, WavFile::WAV_MONO, consts::sampleRate );
}

void FileWriter::closeFile(int in, int out)
{
    std::string fileFullPath = fileMatrix [in][out].getFullPath();
    fileMatrix [in][out].close();
    mvFile(fileFullPath, fileFullPath + ".wav");
}

void FileWriter::updateMatrix(const IOMatrix &ioMatrix){
    memcpy(this->ioMatrix, ioMatrix, sizeof(IOMatrix));
    
    int led_on = LOW;
    for(int i=0; i<consts::inputChannelCount; i++){
        for(int j=0; j<consts::outputChannelCount; j++){
            if(ioMatrix[i][j]){
                led_on = HIGH;
                break;
            }
        }
    }
    
    digitalWrite(1,led_on);
};

bool FileWriter::readMatrix(int in, int out)
{
    return ioMatrix[in][out];
}

void FileWriter::writeAudio(int in, int out, const AudioBlock &audioContainer)
{
    for(int a=0; a<consts::sampleCount; a++)
    {
        std::uint16_t value = audioContainer[a][consts::outputChannelCount - 1 - out];
        fileMatrix [in][out].write_mono_16bit_unsigned(value);
    }
}

bool FileWriter::write(const AudioBlock &audioContainer){
    bool good = true;
    for (unsigned int in = 0; in < consts::inputChannelCount; ++in){
        for (unsigned int out = 0; out < consts::outputChannelCount; ++out){
                if ( readMatrix (in,out) ){//
                    if ( fileMatrix [in][out].is_open()){
                        writeAudio(in,out,audioContainer);

                        if (fileMatrix [in][out].getSampleCount() > 4 * 60 * consts::sampleRate){
                                cout << "Reopen " << in << " - " << out << " = " << fileMatrix [in][out].getSampleCount() << endl;
                                closeFile(in, out);
                                openFile(in, out);
                        }
                    }
                    else
                    {
                        openFile(in, out);
                        cout << "Open " << in << " - " << out << endl;
                        writeAudio(in,out,audioContainer);
                    }
                }
                else{
                    if ( fileMatrix [in][out].is_open())
                    {
                        double tm = fileMatrix [in][out].tmFromOpen();
                        double rate = tm>0 ? fileMatrix [in][out].getSampleCount()/tm : 0;
                        cout << "Close " << in << " - " << out << " = " << rate << endl;
                        closeFile(in, out);
                    }else{
                    }
                }
                
                if(!fileMatrix[in][out])
                    good = false;
        }
    }

//        gettimeofday(&tv,0);
//        uint32_t tm1 = tv.tv_sec*1000000 + tv.tv_usec;
//        cout << "1 " << (tm1 - tm0) << endl;
    
    return good;
}

void FileWriter::createFolderRecursive(std::string folder){
	std::stringstream command;
	command << "mkdir -p " << folder;
	std::system(command.str().c_str());
}

void FileWriter::createArchieveAndDeleteAsync(const std::string &filePath, const std::string &folder){
	std::stringstream command;
    command 
    << "cd " << folder << " && "
    << "tar cvf - " << filePath << " | "
    << "gzip -9 > " << filePath << ".tar.gz && "
	//<< "xz -zf - > " << filePath << ".tar.xz && "

	//<< "bunzip2 -9 > " << filePath << ".tar.bz2 && "
	//<< "xz -9 > " << filePath << ".tar.xz && "
    << "rm -f " + filePath + " &";

    std::system(command.str().c_str());
};


time_t FileWriter::getCurrentTime(){
	time_t rawtime;
	time (&rawtime);
	return rawtime;
}

std::string FileWriter::getWavFileFolder(const time_t &rawTime, const int &in, const int &out){
	char buffer[80];
	struct tm *timeinfo = localtime(&rawTime);
	strftime(buffer,80,"%d-%m-%Y",timeinfo);
	std::string inStr = std::to_string (in+1);
	if (inStr.size() == 1){
		inStr = "0" + inStr;
	}

	std::string outStr = std::to_string (out+1);
	if (outStr.size() == 1){
		outStr = "0" + outStr;
	}

	return folderPath + std::string (buffer) + "/in-" + inStr  + "/out-" + outStr;
};

std::string FileWriter::getWavFileName(const time_t &rawTime, const int &in,const int &out){
	std::stringstream fileName;

	char buffer[300];
	struct tm *timeinfo = localtime(&rawTime);
	strftime(buffer,300,"%d%m%Y_%H%M%S",timeinfo);

	std::string inStr = std::to_string (in+1);
	if (inStr.size() == 1){
		inStr = "0" + inStr;
	}

	std::string outStr = std::to_string (out+1);
	if (outStr.size() == 1){
		outStr = "0" + outStr;
	}

	fileName << buffer << "_" << inStr << "_" << outStr;
	return fileName.str();
}

unsigned long FileWriter::getFreeSpace(const std::string &path){
	//http://stackoverflow.com/questions/15952283/get-real-free-usable-space

	struct statvfs diskData;
	statvfs(path.c_str(), &diskData);
	unsigned long available = diskData.f_bavail * diskData.f_bsize;
	return rounddiv(available, 1024*1024);
}

unsigned long FileWriter::rounddiv(unsigned long num, unsigned long divisor) {
    return (num + (divisor/2)) / divisor;
}

void FileWriter::deleteOldFiles(){
	int minSpaceMb = 50;
	//std::cout << "start get freespace: " << std::endl;
	auto freeSpace = getFreeSpace("/");
	//std::cout << "Freespace: " << freeSpace << std::endl;
 
	if (minSpaceMb > freeSpace){
		std::stringstream command;
		command << "find " << folderPath << "* -type f -mtime +0 -exec rm {} \\;";
		//std::cout << command.str().c_str();
		std::system(command.str().c_str());
	}
}

void FileWriter::saveCurrentTime(){
	std::stringstream command;
	command << "date '+%Y-%m-%d %H:%M:%S' > " << timeFilePath;
	std::system(command.str().c_str());
}


void FileWriter::mvFile(const std::string &from, const std::string &to){
	std::stringstream command;
	command << "mv " << from << " " << to;
	std::system(command.str().c_str());
}

void FileWriter::setFailure(uint8_t new_failure)
{
    if(new_failure != failure)
    {
        failure = new_failure;
        
        cout << "Failure " << failure << endl;
        
        std::ofstream logfile(logPath, std::ofstream::out | std::ofstream::app);
        std::time_t now = std::time(NULL);
        std::tm* tm = std::localtime(&now);
            logfile << setfill('0') << setw(2) << tm->tm_mday << "." << setfill('0') << setw(2) << tm->tm_mon+1 << "." << setfill('0') << setw(4) << tm->tm_year+1900 << " " << tm->tm_hour << ":" << tm->tm_min << ":" << tm->tm_sec << " Failure-" << std::hex << setfill('0') << setw(2) << (int)failure << std::dec << endl;
    }
}

//bool FileWriter::test_matrix(double tm)
//{
////    cout << tm << endl;
//#ifdef __TEST
//    if(tm > 6000){
//        for(int i=0; i<1; i++)
//            ioMatrix[0] &= ~(1<<i);
//        return true;
//    }
//    else if(tm > 1000){
//        for(int i=0; i<1; i++)
//            ioMatrix[0] |= (1<<i);
//    }
//    return false;
//#endif
//}

