#include "consts.h"
#include "converter.h"
#include "fileWriter.h"
#include "ioMatrix.h"
#include "rawParser.h"
#include "reverse.h"
#include "stm.h"
#include "UARTThread.h"
#include "HardwareMutex.h"
#include "audioContainer.h"
#include "StateSender.h"
#include "Player.h"

#include <wiringPi.h>
#include <wiringPiSPI.h>

#include <chrono>
#include <iostream>
#include <fstream>
#include <string>
#include <sys/auxv.h>
#include <bitset>
#include <chrono>
#include <mutex>
#include <arpa/inet.h>
#include <sys/time.h>
#include <math.h>

double getTm()
{
    timeval tv;
    gettimeofday(&tv,0);
    return (double)tv.tv_sec*1000.0 + (double)tv.tv_usec/1000.0;
}

int main (int argc, char **argv){
        cout << "Start " << consts::rawDataSize << endl;
	using namespace consts;
//	std::string pathToExe = std::string((char*)getauxval(AT_EXECFN));
	std::string folderPath = "/etc/bksarec";//pathToExe.substr(0, pathToExe.find_last_of("/\\"));
	std::string version = "1.3.0";

	{
		std::string pathToVersionFile = folderPath + "/" + std::string("version.txt");
		std::ofstream file(pathToVersionFile);
		file << version;
	}


	std::string pathToWavFiles;
	if (argc > 1)
		pathToWavFiles = argv[1];
	else{
		pathToWavFiles = "/home/pi/FTP/rec/";
	}
        
        std::string pathToLog = "/home/pi/FTP/rec/log.txt";
        
        string serverIP = "";
        int serverPort = 0;
        {
            ifstream ifIP("/etc/bksarec/server-ip.conf");
            ifIP >> serverIP;
            
            ifstream ifPort("/etc/bksarec/server-port.conf");
            ifPort >> serverPort;
        }

	std::cout << "BKS wav started" << "\n" <<
	"Path to wav files: " << pathToWavFiles << "\n" <<
        "Server: " << serverIP << ":" << serverPort << "\n" <<
	"version " <<  version << "\n" <<
	"----------------------" << "\n";


	std::string pathToTime = folderPath + "/" + std::string("clock.txt");

	wiringPiSetup();
	for (const auto &channel: consts::alarmLines){
		pinMode(channel, INPUT);
	}

	RawParser rawParser;
	FileWriter fileWriter(pathToWavFiles, pathToTime, pathToLog);
	Stm stm(chipSelectPin,dataAvaiblePin,spiChannel, spiSpeed);
	if (!stm.init()){
		std::cout << "Stm  init failed" << "\n";
		return 1;
	};
        StateSender stateSender(serverIP,serverPort);
        
        Player player(folderPath);
        
	std::cout << "Start loop" << "\n";
        
//        digitalWrite(consts::raspReadyPin, HIGH);
        
        int audCnt = 0;
        uint16_t lastN = 0;
        int emptyCnt = 2;
        double tm = getTm();
        double tm0 = tm;
        double tmWrk = tm;
        double tmStart = tm;
        double lastTm = tm;
        
	while(true){
            if (stm.dataAvailable())//
            {
                lastTm = tm;
                tm = getTm();
                
                RawData rawData;
                stm.getRawData(rawData);
                
                bool matr = rawParser.isMatrix(rawData);
                bool aud = rawParser.isAudio(rawData);
                bool valid = matr && aud;
                stm.nextSpiPacketWithFF(!valid);
                
                AudioBlock audio;
                if(valid)
                {
                    for (auto &v: rawData.matrix){
                            v = reverseHiByte(v);
                    }

                    IOMatrix matrix;
                    uint8_t failure;
                    rawParser.getIOMatrix(rawData, matrix, failure);

                    fileWriter.updateMatrix(matrix);
//                    fileWriter.setFailure(failure);
                    stateSender.setRawData(rawData);
//                    fileWriter.test_matrix(tm - tmStart);
                    
                    for(int i=0; i<consts::sampleCount; i++)
                    {
                        rawParser.getAudio(rawData, i, audio[i]);
                    }
                }
                else
                {
                    for(int i=0; i<consts::sampleCount; i++)
                        getEmptyAudio(audio[i]);
                    
                    if(!matr)
                        cerr << "M";
                    else if(!aud)
                        cerr << "A";
                }
                if(!fileWriter.write (audio))
                    stm.nextSpiPacketWithFF(true);
    

//                if((tm - tmWrk) > 5000)
//                {
//                    worker.wake();
//                    tmWrk = tm;
//                }
            }
//            else
//                cout << ".";
            
            player.run();
	}
	return 0;
}
