#!/bin/bash

raspi-config

apt update
apt -y install mc gcc g++ make gdb libopus-dev wiringpi git pure-ftpd ntp omxplayer

git clone https://gitlab.com/mclaught/BKSARec.git
cd BKSARec
make
cp -f dist/Debug/GNU-Linux/bksarec /usr/sbin
chmod 755 /usr/sbin/bksarec
cp -f bksarec.d /etc/init.d
chmod 755 /etc/init.d/bksarec.d
cp -R etc /
update-rc.d bksarec.d defaults
service bksarec.d start

cd ..
groupadd ftpgroup
useradd ftpuser -g ftpgroup -s /sbin/nologin -d /dev/null
mkdir /home/pi/FTP
chown -R ftpuser:ftpgroup /home/pi/FTP
pure-pw useradd pi -u ftpuser -g ftpgroup -d /home/pi/FTP -m
pure-pw mkdb
ln -s /etc/pure-ftpd/conf/PureDB /etc/pure-ftpd/auth/10puredb
service pure-ftpd restart

reboot