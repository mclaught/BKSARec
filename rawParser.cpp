#include <iostream>

#include "rawParser.h"

using namespace std;

bool RawParser::isAudio (const RawData &rawData) const{
//    return rawData.type == RDQIT_AUDIO;
	std::uint16_t matrixBitMask = 0;

//        int n=0;
	int chC = consts::outputChannelCount;
	for (int i=0; i < chC; ++i){
            for(int j=0; j<consts::sampleCount; j++){
//                if(n < 10)
//                    cout << hex << rawData.audio[i + 16*j] << " ";
		int ch = (rawData.audio[i + 16*j] >> 12 ) & 0x000F;
		if (ch != i){
			return false;
		}
            }
	}

	return true;
};
bool RawParser::isMatrix (const RawData &rawData) const{
//    return rawData.type == RDQIT_MATRIX;
	uint64_t matrixBitMask = 0;

	for (const uint16_t &row: rawData.matrix){
//                uint16_t value = (0xFF00 & row);
//                if(value)
                
		bool lastBitIsOne = row & 0x1;
		if (!lastBitIsOne){
//                    cout << "m";
                    return false;
		}
		uint16_t position = row & 0x00FE;
		position >>= 1LL;
//                cout << position << " ";
		matrixBitMask |= 1LL << position;
	}
        
//        cout << endl;
        
        uint16_t fail_num = rawData.failure & 0x00FF;
        if(fail_num != 0x0045)
        {
            cout << "F" << hex << rawData.failure << dec << endl;
            return false;
        }

	const uint64_t referenceMask = 0x3FFFFFFFF;
	return matrixBitMask == referenceMask;
};

void RawParser::getIOMatrix (const RawData &rawData, IOMatrix &ioMAtrix, uint8_t &failure) const{
//	IOMatrix ioMAtrix;
	for (auto &row: ioMAtrix){
		for (auto &value: row){
			value = false;
		};
	};

	for (const uint16_t &row: rawData.matrix){
//                cout << hex << row << " ";
            
		uint16_t byteNum = (0x00FE & row);
		byteNum >>= 1LL;

		uint16_t value = (0xFF00 & row);
		value >>= 8LL;

		bool isFirstPartChannels = (byteNum%2 == 0);
		int channelNum = (byteNum)/2;
                
		for(int i=0; i<8; ++i){
			bool chValue = (value >> (7-i)) & 1;

			if (isFirstPartChannels){
				ioMAtrix[channelNum][i] = chValue;
			}
			else{
				ioMAtrix[channelNum][i+8] = chValue;
			}
		};
                
//                cout << dec << channelNum << "-" << hex << value << " ";
	}
        
        failure = (rawData.failure & 0xFF00) >> 8;
//        cout << endl;
};

void RawParser::getAudio(const RawData &rawData, const int &index, AudioContainer &audioData) const{
//	AudioContainer audioData;
	const int indexAdd = 16*index;
	for (int i=0; i< consts::outputChannelCount; ++i){
		int index = 16 * i;
		std::uint16_t data = rawData.audio [i + indexAdd] & 0x0FFF;
		audioData[i] = convert12to16BitValue (data);
	}

//	return audioData;
};
