#pragma once
#include <cstdint>

inline std::uint16_t convert12to16BitValue(std::uint16_t value){
	const std::uint16_t max16bit = 0xFFFF;
	const std::uint16_t max12bit = 0x0FFF;
	const std::uint16_t coef = max16bit / max12bit;

	if (value > max12bit)
		return value;
	else
		return value*coef;
}