#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#define PLAYER_PIN  7

using namespace std;

class Player {
public:
    Player(string conf_path);
    Player(const Player& orig);
    virtual ~Player();
    
    void run();
private:
//    string filePath = "/home/pi/FTP/play/play.mp3";
    int last_ctrl = 0;
    pid_t pid = -1;
};

#endif /* PLAYER_H */

