/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HardwareMutex.h
 * Author: user
 *
 * Created on 7 апреля 2017 г., 16:20
 */

#ifndef HARDWAREMUTEX_H
#define HARDWAREMUTEX_H

class HardwareMutex {
public:
    HardwareMutex(int _pin);
    HardwareMutex(const HardwareMutex& orig);
    virtual ~HardwareMutex();
private:
    int pin;
};

#endif /* HARDWAREMUTEX_H */

