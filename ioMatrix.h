#pragma once
#include "consts.h"

#include <array>

//typedef uint16_t IOMatrix[consts::inputChannelCount];
typedef bool IOMatrix[consts::inputChannelCount][consts::outputChannelCount];
//typedef std::array <std::array <bool, consts::outputChannelCount>, consts::inputChannelCount> IOMatrix;
