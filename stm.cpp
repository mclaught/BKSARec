#include <errno.h>
#include <fcntl.h> 
#include <iostream>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>

#include "stm.h"

using namespace std;

Stm::Stm(int chipSelectPin, int dataAvaiblePin, int spiChannel, int speed){
	this->dataAvaiblePin = dataAvaiblePin;
	this->chipSelectPin = chipSelectPin;
	this->spiChannel = spiChannel;

	wiringPiSPISetup(spiChannel,speed);
        
	pinMode(dataAvaiblePin, OUTPUT);
	sendFF = false;
};

void Stm::getRawData(RawData &tempArray){
//	RawData tempArray;

//	for (auto &value: tempArray.matrix){
//		if (sendFF)
//			value = 0xFFFF;
//		else
//			value = 0x0000;
//	}
//        
//        if (sendFF)
//                tempArray.failure = 0xFFFF;
//        else
//                tempArray.failure = 0x0000;
//        
//	for (auto &value: tempArray.audio){
//		if (sendFF)
//			value = 0xFFFF;
//		else
//			value = 0x0000;
//	}
    
        if(sendFF)
        {
            memset(&tempArray,0xFF,consts::rawDataSize);
        }
        else
        {
            memset(&tempArray,0x00,consts::rawDataSize);
        }

	setGpio(false);
	wiringPiSPIDataRW(spiChannel, (unsigned char*) &tempArray, consts::rawDataSize);//
	setGpio(true);
        
	for (std::uint16_t &value: tempArray.matrix){
//		std::uint16_t hiByte = (value & 0xFF00) >> 8;
//		std::uint16_t loByte = (value & 0xFF);
//		value = loByte << 8 | hiByte;
            value = ntohs(value);
	}
        tempArray.failure = ntohs(tempArray.failure);
	for (std::uint16_t &value: tempArray.audio){
//		std::uint16_t hiByte = (value & 0xFF00) >> 8;
//		std::uint16_t loByte = (value & 0xFF);
//		value = loByte << 8 | hiByte;
            value = ntohs(value);
//                cout << hex << value << " ";
	}

//	return tempArray;
};

bool Stm::dataAvailable(){
	return digitalRead (dataAvaiblePin) == HIGH;
};

bool Stm::init(){
    
	mem_fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (0 > mem_fd){
                cerr << "/dev/mem open failed" << endl;
		return false;
	};

	const int BLOCK_SIZE = 4*1024;
	const int PAGE_SIZE = 4*1024;
	const int GPIO_BASE = 0x20000000 + 0x20000;

	gpio_map = mmap (
		NULL,
		BLOCK_SIZE,
		PROT_READ | PROT_WRITE,
		MAP_SHARED,
		mem_fd,
		GPIO_BASE
	);

	close(mem_fd);

	if (gpio_map == MAP_FAILED){
                cerr << "GPIO map failed" << endl;
		return false;
	};

	gpio = (volatile unsigned*) gpio_map;

	const int pin = 8;
	const int  avPin = 23;

	int add =  pin/10;
	int sadd = (pin%10)*3;

	int addTwo = avPin/10;
	int saddTwo = (avPin%10)*3;

	*(gpio + add) 		&= ~(7 << sadd);
	*(gpio + add) 		|=  (1 << sadd);
	//*(gpio + addTwo)	&= ~(7 << saddTwo);

	return true;
}

void Stm::nextSpiPacketWithFF(bool value){
	sendFF = value;
}


void Stm::setGpio(bool set){
	const int pin = 8;
	if (set){
		*(gpio + 7) = 1 << pin;
	}
	else{
		*(gpio + 10) = 1 << pin;
	}
};


int Stm::set_interface_attribs (int fd, int speed, int parity)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
            cerr << "error " << errno << " from tcgetattr" << endl;
            return -1;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
            cerr << "error " << errno << " from tcsetattr" << endl;
            return -1;
    }
    return 0;
}

bool Stm::set_blocking (int fd, int should_block)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
            cerr << "error " << errno << " from tcgetattr" << endl;
            return false;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        cerr << "error " << errno << " from tcsetattr: " << strerror(errno) << endl;
        return false;
    }
    
    return true;
}

