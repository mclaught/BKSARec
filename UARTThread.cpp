/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UARTThread.cpp
 * Author: user
 * 
 * Created on 16 марта 2017 г., 10:55
 */
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <wiringPi.h>

#include "UARTThread.h"
#include "reverse.h"

UARTThread* uart_thread;

UARTThread::UARTThread(RawParser *_rawParser, FileWriter *_fileWriter, Stm *_stm) : 
    MyThread(), rawParser(_rawParser), fileWriter(_fileWriter), stm(_stm)
{
}

UARTThread::UARTThread(const UARTThread& orig)
{
}

UARTThread::~UARTThread()
{
}

uint64_t UARTThread::getMillis()
{
    timeval tv;
    gettimeofday(&tv,0);
    return tv.tv_sec*1000 + tv.tv_usec/1000;
}

void UARTThread::exec()
{
    while(!terminated)
    {
        RawData rawData = stm->getRawDataUART();
        
        if (rawParser->isMatrix(rawData)){
            for (auto &v: rawData){
                    v = reverseHiByte(v);
            }

            auto matrix = rawParser->getIOMatrix(rawData);
            
            {
                std::unique_lock<std::mutex> locker(fw_mutex);
                fileWriter->updateMatrix(matrix);
            }
        }
    }
}
