#pragma once
#include "audioContainer.h"
#include "consts.h"
#include "ioMatrix.h"
#include "wav.h"
#include <mutex>

#include <array>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <sys/statvfs.h>

#define __TEST

typedef WavFile FileMatrix[consts::inputChannelCount][consts::outputChannelCount];

class FileWriter{
//	using FileMatrix = std::array< std::array <WavFile, consts::outputChannelCount>, consts::inputChannelCount>;

        FileMatrix fileMatrix;
	IOMatrix ioMatrix;
	std::string folderPath;
	std::string timeFilePath;
        std::string logPath;
        uint8_t failure;
private:
	time_t getCurrentTime();
	unsigned long rounddiv(unsigned long num, unsigned long divisor);
	void createArchieveAndDeleteAsync(const std::string &filePath, const std::string &folder);
	void createFolderRecursive(std::string folder);
	void saveCurrentTime();
        void openFile(int in, int out);
        void closeFile(int in, int out);
        bool readMatrix(int in, int out);
public:
	FileWriter(const std::string &pathToWav, const std::string &pathToTime, const std::string pathToLog);

	std::string getWavFileFolder(const time_t &rawTime, const int &in, const int &out);
	std::string getWavFileName(const time_t &rawTime, const int &in,const int &out);
	
	unsigned long getFreeSpace(const std::string &path);
	void mvFile(const std::string &from, const std::string &to);

	void deleteOldFiles();
	void updateMatrix(const IOMatrix &ioMatrix);
        void writeAudio(int in, int out, const AudioBlock &audioContainer);
	bool write(const AudioBlock &audioContainer);
        
        void setFailure(uint8_t new_failure);
//        bool test_matrix(double tm);
};

extern std::mutex fw_mutex;