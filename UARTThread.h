/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UARTThread.h
 * Author: user
 *
 * Created on 16 марта 2017 г., 10:55
 */

#ifndef UARTTHREAD_H
#define UARTTHREAD_H

#include <termios.h>
#include <mutex>
#include <array>

#include "MyUtils/MyThread.h"
#include "fileWriter.h"
#include "rawParser.h"
#include "stm.h"

#define LINE_ON 12
#define BKS_EN 13

using namespace std;

class UARTThread : public MyThread {
public:
    UARTThread(RawParser *_rawParser, FileWriter *_fileWriter, Stm *_stm);
    UARTThread(const UARTThread& orig);
    virtual ~UARTThread();
private:
    RawParser *rawParser;
    FileWriter *fileWriter;
    Stm *stm;
    bool sendFF = false;
    
    virtual void exec();
    uint64_t getMillis();
};

extern UARTThread* uart_thread;

#endif /* UARTTHREAD_H */

