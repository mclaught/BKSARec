#!/bin/bash

ver="1.1.1"
curver=`cat /etc/bksarec/version.txt`
server="192.168.1.252"
user="mclaught"
pass="svtapi71"
path="ftp://$user:$pass@$server/bksarec/bksarec"

echo "$curver -> $ver"

if [ $curver == $ver ]
then
    echo "Already up to date"
else
    echo "Do update"

    wget $path
    service bksarec.d stop
    cp -f bksarec /usr/sbin
    chmod 755 /usr/sbin/bksarec
    rm -f bksarec
    service bksarec.d start
fi
