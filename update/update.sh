#!/bin/bash

apt update
apt upgrade
apt install -y omxplayer wiringpi gcc g++ make
rm -r BKSARec
git clone https://mclaught@gitlab.com/mclaught/BKSARec.git
cd BKSARec
make CONF=Release

service bksarec.d stop

cp -f dist/Release/GNU-Linux/bksarec /usr/sbin
cp -f bksarec.d /etc/init.d
chmod 755 /usr/sbin/bksarec
chmod 755 /etc/init.d/bksarec.d
systemctl daemon-reload

service bksarec.d start

rm -r /home/pi/FTP/*
mkdir -p /home/pi/FTP/rec
mkdir -p /home/pi/FTP/play
chown -R ftpuser:ftpgroup /home/pi/FTP
