#pragma once
#include "audioContainer.h"
#include "consts.h"
#include "converter.h"
#include "ioMatrix.h"
#include "stm.h"

#include <array>
#include <bitset>
#include <iostream>
#include <vector>

class RawParser{
public:
	bool isAudio (const RawData &rawData) const;
	bool isMatrix(const RawData &rawData) const;
        
	void getIOMatrix(const RawData &rawData, IOMatrix &ioMAtrix, uint8_t &failure) const;
	void getAudio(const RawData &rawData, const int &index, AudioContainer &audioData) const;
};
