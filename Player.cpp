#include "Player.h"
#include "wiringPi.h"
#include <fstream>
#include <filesystem>
#include <unistd.h>
#include <signal.h>
#include <set>

Player::Player(string conf_path) {
    pinMode(PLAYER_PIN, INPUT);
    pullUpDnControl(PLAYER_PIN, PUD_DOWN);
    
//    ifstream ifConf(conf_path+"/play_file.conf");
//    if(ifConf.good()){
//        ifConf >> filePath;
//    }
    
    printf("Player started\n");
}

Player::Player(const Player& orig) {
}

Player::~Player() {
}

void Player::run(){
    auto ctrl = digitalRead(PLAYER_PIN);
    if(ctrl != last_ctrl){
        if(ctrl){
            pid = fork();
            if(pid > 0){
                printf("Create process: %d\n", pid);
            }else if(pid == 0){
                set<string> files;
                for(auto entry : filesystem::directory_iterator("/home/pi/FTP/play")){
                    files.insert(entry.path().native());
                }
                
                while(true){
                    for(auto filePath : files){
                        printf("Play file: %s\n", filePath.c_str());
                        string cmd = "omxplayer \""+filePath+"\"";
                        printf("%s\n", cmd.c_str());
                        system(cmd.c_str());
                    }
                }
            }else{
                printf("Create process error");
            }
            
        }else{
            printf("Stop play: %d\n", pid);
            if(pid > 0){
                kill(pid, SIGKILL);
                pid = -1;
            }
            system("killall -s9 omxplayer.bin");
            system("killall -s9 omxplayer");
        }
        
        last_ctrl = ctrl;
    }
}

