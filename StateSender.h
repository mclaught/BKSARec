/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StateSender.h
 * Author: user
 *
 * Created on 4 мая 2017 г., 9:22
 */

#ifndef STATESENDER_H
#define STATESENDER_H

#include "stm.h"

const int sendSize = (consts::inputChannelCount*2*sizeof(uint16_t) + sizeof(uint16_t));

using namespace std;

class StateSender {
public:
    StateSender(string ip, uint16_t port);
    StateSender(const StateSender& orig);
    virtual ~StateSender();
    
//    void setFailure(uint8_t new_failure);
    void setRawData(RawData &rawData);
private:
    int sock;
    string _ip;
    uint16_t _port;
//    uint8_t failure;
    uint32_t tm_last_send;
    RawData rawData;
    
    void sendState();
    uint32_t getTime();
};

#endif /* STATESENDER_H */

