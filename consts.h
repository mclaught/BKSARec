#pragma once
#include <vector>
#include <string>

#define DISPLAY

namespace consts{
	//audio
	const static int sampleRate = 8000;
	const static int inputChannelCount = 17;
	const static int outputChannelCount = 16;

	//STM
	const static int dataAvaiblePin = 4;
	const static int chipSelectPin = 10;
        const static int raspReadyPin = 0;

	const static int spiChannel = 0;
	const static int spiSpeed = 8000000;
        const static int sampleCount = 100;
	const static int blockSize = sampleCount*outputChannelCount;
        const static int rawDataSize = blockSize*2+68+2;

	//alarm
	const static std::vector<int> alarmLines = {
		5, 11, 21, 22, 26, 23, 24, 25
	};
        
        const static int queueSize = 1000000;
};
