/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HardwareMutex.cpp
 * Author: user
 * 
 * Created on 7 апреля 2017 г., 16:20
 */
#include <iostream>

#include "wiringPi.h"
#include "HardwareMutex.h"
#include "consts.h"

using namespace std;

HardwareMutex::HardwareMutex(int _pin) : pin(_pin)
{
#ifdef DISPLAY
    cout << "HM: LOW" << endl;
#endif
    digitalWrite(pin, LOW);
}

HardwareMutex::HardwareMutex(const HardwareMutex& orig) {
}

HardwareMutex::~HardwareMutex() 
{
#ifdef DISPLAY
    cout << "HM: HIGH" << endl;
#endif
    digitalWrite(pin, HIGH);
}

