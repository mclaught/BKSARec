/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   gpiotest.cpp
 * Author: user
 *
 * Created on 20 апреля 2017 г., 21:59
 */

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <iostream>

#include "wiringPi.h"
#include "wiringPiSPI.h"

/*
 * Simple C++ Test Suite
 */
using namespace std;

uint8_t data[100];

void test1() {
    std::cout << "gpiotest test 1" << std::endl;
    
    
    for(int i=0;i<10;i++)
    {
        for(auto &v : data)
            v = 0x55;
        
//        digitalWrite(12,HIGH);
        wiringPiSPIDataRW(0,data,100);
        usleep(500000);
        cout << "ON" << endl;
        
//        digitalWrite(12,LOW);
//        usleep(250000);
//        cout << "OFF" << endl;
    }
}

void test2() {
    std::cout << "gpiotest test 2" << std::endl;
    std::cout << "%TEST_FAILED% time=0 testname=test2 (gpiotest) message=error message sample" << std::endl;
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% gpiotest" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;
    
//    wiringPiSetup();
//    pinMode(12,OUTPUT);
    wiringPiSPISetupMode(0,1000,0);

    std::cout << "%TEST_STARTED% test1 (gpiotest)" << std::endl;
    test1();
    std::cout << "%TEST_FINISHED% time=0 test1 (gpiotest)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

