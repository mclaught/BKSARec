/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   stmtest.cpp
 * Author: user
 *
 * Created on 14 апреля 2017 г., 12:48
 */

#include <stdlib.h>
#include <iostream>
#include <sys/time.h>

#include "consts.h"
#include "wiringPi.h"
#include "stm.h"

/*
 * Simple C++ Test Suite
 */

void test1() {
    std::cout << "stmtest test 1" << std::endl;
}

void test2() {
    std::cout << "stmtest test 2" << std::endl;
    std::cout << "%TEST_FAILED% time=0 testname=test2 (stmtest) message=error message sample" << std::endl;
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% stmtest" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;
    
    wiringPiSetup();
    std::cout << "wiringPiSetup()" << std::endl;
    for (const auto &channel: consts::alarmLines){
            pinMode(channel, INPUT);
    }
    Stm stm(consts::chipSelectPin, consts::dataAvaiblePin, consts::spiChannel, consts::spiSpeed);
    std::cout << "Stm stm" << std::endl;
    if (!stm.init()){
        std::cout << "%TEST_FAILED% time=0 testname=test1 (stmtest) message=STM init failed" << std::endl;
    };
    std::cout << "Stm init" << std::endl;

    std::cout << "%TEST_STARTED% test1 (stmtest)" << std::endl;
    
    timeval tv;
    gettimeofday(&tv,0);
    double tm0 = tv.tv_sec*1000+tv.tv_usec/1000.0;
    
    int n = 0;
    for(int i=0; i<1000000; i++) {
        if (stm.dataAvailable())//
        {
            RawData rawData;
            stm.getRawData(rawData);
            std::cout << ".";
            n++;
        }
    }
    gettimeofday(&tv,0);
    double tm1 = tv.tv_sec*1000+tv.tv_usec/1000.0;
    
    std::cout << std::endl << n << std::endl;
    
    std::cout << "%TEST_FINISHED% time=" << (tm1-tm0) << " test1 (stmtest)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

