/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   wavtest.cpp
 * Author: user
 *
 * Created on 13 апреля 2017 г., 19:05
 */

#include <stdlib.h>
#include <iostream>
#include <sys/time.h>
#include <sstream>

#include "wav.h"

/*
 * Simple C++ Test Suite
 */

using namespace std;

void test1() {
    std::cout << "%TEST_STARTED% wav (wavtest)" << std::endl;
    std::cout << "wavtest test 1" << std::endl;
    
    uint16_t data = 0xA5A5;
    WavFile wavs[16];
    int n = 1;
    for(auto &wav : wavs)
    {
        ostringstream stm;
        stm << "test" << n++ << ".wav";
        wav.open(stm.str(),"/home/pi/FTP",WavFile::WAV_16BIT, WavFile::WAV_MONO, 8000);
    }
    
    timeval tv;
    gettimeofday(&tv,0);
    double tm0 = (double)tv.tv_sec + (double)tv.tv_usec/1000000.0;
    
    for(int i=0; i<1*1024*1024; i++)
    {
        for(auto &wav : wavs)
            wav.write_mono_16bit(data);
    }

    gettimeofday(&tv,0);
    double tm1 = (double)tv.tv_sec + (double)tv.tv_usec/1000000.0;
    
    for(auto &wav : wavs)
        wav.close();
    
    std::cout << "%TEST_FINISHED% time=" << (tm1-tm0) << " wav (wavtest)" << std::endl;
}

void test2() {
    std::cout << "wavtest test 2" << std::endl;
    std::cout << "%TEST_FAILED% time=0 testname=test2 (wavtest) message=error message sample" << std::endl;
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% wavtest" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    
    test1();
    

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

