#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/MyUtils/MyEvent.o \
	${OBJECTDIR}/MyUtils/MyThread.o \
	${OBJECTDIR}/Player.o \
	${OBJECTDIR}/StateSender.o \
	${OBJECTDIR}/fileWriter.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/rawParser.o \
	${OBJECTDIR}/stm.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f4 \
	${TESTDIR}/TestFiles/f3 \
	${TESTDIR}/TestFiles/f2

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/tests/gpiotest.o \
	${TESTDIR}/tests/stmtest.o \
	${TESTDIR}/tests/wavtest.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++17
CXXFLAGS=-std=c++17

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lwiringPi -lpthread -lstdc++fs

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/bksarec

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/bksarec: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/bksarec ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/MyUtils/MyEvent.o: MyUtils/MyEvent.cpp
	${MKDIR} -p ${OBJECTDIR}/MyUtils
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MyUtils/MyEvent.o MyUtils/MyEvent.cpp

${OBJECTDIR}/MyUtils/MyThread.o: MyUtils/MyThread.cpp
	${MKDIR} -p ${OBJECTDIR}/MyUtils
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MyUtils/MyThread.o MyUtils/MyThread.cpp

${OBJECTDIR}/Player.o: Player.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Player.o Player.cpp

${OBJECTDIR}/StateSender.o: StateSender.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/StateSender.o StateSender.cpp

${OBJECTDIR}/fileWriter.o: fileWriter.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/fileWriter.o fileWriter.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/rawParser.o: rawParser.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rawParser.o rawParser.cpp

${OBJECTDIR}/stm.o: stm.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/stm.o stm.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f4: ${TESTDIR}/tests/gpiotest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f4 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f3: ${TESTDIR}/tests/stmtest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f2: ${TESTDIR}/tests/wavtest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f2 $^ ${LDLIBSOPTIONS}   


${TESTDIR}/tests/gpiotest.o: tests/gpiotest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/gpiotest.o tests/gpiotest.cpp


${TESTDIR}/tests/stmtest.o: tests/stmtest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/stmtest.o tests/stmtest.cpp


${TESTDIR}/tests/wavtest.o: tests/wavtest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -IwiringPi -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/wavtest.o tests/wavtest.cpp


${OBJECTDIR}/MyUtils/MyEvent_nomain.o: ${OBJECTDIR}/MyUtils/MyEvent.o MyUtils/MyEvent.cpp 
	${MKDIR} -p ${OBJECTDIR}/MyUtils
	@NMOUTPUT=`${NM} ${OBJECTDIR}/MyUtils/MyEvent.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -IwiringPi -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MyUtils/MyEvent_nomain.o MyUtils/MyEvent.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/MyUtils/MyEvent.o ${OBJECTDIR}/MyUtils/MyEvent_nomain.o;\
	fi

${OBJECTDIR}/MyUtils/MyThread_nomain.o: ${OBJECTDIR}/MyUtils/MyThread.o MyUtils/MyThread.cpp 
	${MKDIR} -p ${OBJECTDIR}/MyUtils
	@NMOUTPUT=`${NM} ${OBJECTDIR}/MyUtils/MyThread.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -IwiringPi -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MyUtils/MyThread_nomain.o MyUtils/MyThread.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/MyUtils/MyThread.o ${OBJECTDIR}/MyUtils/MyThread_nomain.o;\
	fi

${OBJECTDIR}/Player_nomain.o: ${OBJECTDIR}/Player.o Player.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Player.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -IwiringPi -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Player_nomain.o Player.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Player.o ${OBJECTDIR}/Player_nomain.o;\
	fi

${OBJECTDIR}/StateSender_nomain.o: ${OBJECTDIR}/StateSender.o StateSender.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/StateSender.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -IwiringPi -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/StateSender_nomain.o StateSender.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/StateSender.o ${OBJECTDIR}/StateSender_nomain.o;\
	fi

${OBJECTDIR}/fileWriter_nomain.o: ${OBJECTDIR}/fileWriter.o fileWriter.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/fileWriter.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -IwiringPi -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/fileWriter_nomain.o fileWriter.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/fileWriter.o ${OBJECTDIR}/fileWriter_nomain.o;\
	fi

${OBJECTDIR}/main_nomain.o: ${OBJECTDIR}/main.o main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -IwiringPi -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main_nomain.o main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/main.o ${OBJECTDIR}/main_nomain.o;\
	fi

${OBJECTDIR}/rawParser_nomain.o: ${OBJECTDIR}/rawParser.o rawParser.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/rawParser.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -IwiringPi -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rawParser_nomain.o rawParser.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/rawParser.o ${OBJECTDIR}/rawParser_nomain.o;\
	fi

${OBJECTDIR}/stm_nomain.o: ${OBJECTDIR}/stm.o stm.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/stm.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -IwiringPi -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/stm_nomain.o stm.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/stm.o ${OBJECTDIR}/stm_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f4 || true; \
	    ${TESTDIR}/TestFiles/f3 || true; \
	    ${TESTDIR}/TestFiles/f2 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
