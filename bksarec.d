#!/bin/sh
### BEGIN INIT INFO
# Provides:          bksarec
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs $network
# Default-Start:     3 4 5
# Default-Stop:      0 1 6
# Short-Description: BKS Audio recorder
# Description:       BKS Audio recorder
### END INIT INFO

# Author: mclaught <sergey@taldykin.com>
#

DESC="BKS Audio recorder"
NAME=bksarec
DAEMON=/usr/sbin/${NAME}

case $1 in
    start|restart)
        echo "Applying settings..."

        if [ -e /boot/eth0 ]; then

            diff -q /boot/eth0 /etc/network/interfaces.d/eth0
            if [ $? -ne 0 ]; then
                cp -f /boot/eth0 /etc/network/interfaces.d
                chmod 644 /etc/network/interfaces.d/eth0
                ifdown eth0 && ifup eth0
            else
                echo 'Files is not different'
            fi
        else
            echo 'File not found eth0'
        fi

        if [ -e /boot/dhcpcd.conf ]; then

            diff -q /boot/dhcpcd.conf /etc/dhcpcd.conf
            if [ $? -ne 0 ]; then
                cp -f /boot/dhcpcd.conf /etc/dhcpcd.conf
                chmod 664 /etc/dhcpcd.conf
                ifdown eth0 && ifup eth0
            else
                echo 'Files is not different'
            fi
        else
            echo 'File not found eth0'
        fi

        if [ -e /boot/server-ip.conf ]; then

            diff -q /boot/server-ip.conf /etc/bksarec/server-ip.conf
            if [ $? -ne 0 ]; then
                cp -f /boot/server-ip.conf /etc/bksarec
                chmod 644 /etc/bksarec/server-ip.conf
            else
                echo 'Files is not different'
            fi
        else
            echo 'File not found server-ip.conf'
        fi

        if [ -e /boot/server-port.conf ]; then

            diff -q /boot/server-port.conf /etc/bksarec/server-port.conf
            if [ $? -ne 0 ]; then
                cp -f /boot/server-port.conf /etc/bksarec
                chmod 644 /etc/bksarec/server-port.conf
            else
                echo 'Files is not different'
            fi
        else
            echo 'File not found server-port.conf'
        fi

        if [ -e /boot/ntp.conf ]; then

            diff -q /boot/ntp.conf /etc/ntp.conf
            if [ $? -ne 0 ]; then
                cp -f /boot/ntp.conf /etc
                chmod 644 /etc/ntp.conf
				service ntp restart
            else
                echo 'Files is not different'
            fi
        else
            echo 'File not found ntp.conf'
        fi

        service ntp status
        if [ $? -eq 0 ]
        then
            service ntp stop
        fi
        ntpdate
        service ntp start

        echo "Starting ${DESC}..."

        cd /
        exec > /dev/null
        exec 2> /dev/null
        exec < /dev/null 

        killall ${NAME}
        .${DAEMON} &
        ;;

    stop)
        echo "Stoping ${DESC}..."
        killall ${NAME}
        ;;

    status)
        pidof ${DAEMON}
        if [ $? -eq 0 ]
        then
            echo "Running"
            exit 0
        else
            echo "Stoped"
            exit 1
        fi
        ;;
esac

