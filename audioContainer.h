#pragma once
#include "consts.h"
#include <array>

//typedef std::array <std::uint16_t, consts::outputChannelCount> AudioContainer;
typedef std::uint16_t AudioContainer[consts::outputChannelCount];
typedef AudioContainer AudioBlock[consts::sampleCount];

inline void getEmptyAudio(AudioContainer &data){
	for (int i=0; i< consts::outputChannelCount; ++i){
		data[i] = 0x7FFF; // half 0xFFFF
	};
};
