#pragma once
#include <cstdint>
#include <cstdio>

inline std::uint8_t reverse (std::uint8_t v){
	v = (v & 0xF0) >> 4 | (v & 0x0F) << 4;
	v = (v & 0xCC) >> 2 | (v & 0x33) << 2;
	v = (v & 0xAA) >> 1 | (v & 0x55) << 1;
	return v;
};

inline std::uint16_t reverseHiByte(std::uint16_t value){
	uint8_t hi = (value & 0xFF00) >> 8;
	uint16_t rHi = reverse (hi);
	uint16_t n = (rHi << 8) | (value & 0x00FF);
	return n;
};
