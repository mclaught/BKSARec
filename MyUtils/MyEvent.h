/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyEvent.h
 * Author: user
 *
 * Created on 8 апреля 2017 г., 11:07
 */

#ifndef MYEVENT_H
#define MYEVENT_H

#define TM_INFINITE -1

#include <mutex>
#include <condition_variable>

class MyEvent {
public:
    MyEvent();
    MyEvent(const MyEvent& orig);
    virtual ~MyEvent();
    
    void wait(int tm = TM_INFINITE);
    void notify(bool all=false);
private:
    std::mutex mut;
    std::condition_variable cv;
};

#endif /* MYEVENT_H */

