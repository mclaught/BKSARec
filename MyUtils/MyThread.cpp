/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyThread.cpp
 * Author: user
 * 
 * Created on 16 января 2017 г., 9:56
 */

#include <sys/syslog.h>
#include <pthread.h>
#include <iostream>
#include <cstring>

#include "MyThread.h"

using namespace std;

MyThread::MyThread() : terminated(false)
{
    _thrd = NULL;
}

MyThread::MyThread(const MyThread& orig)
{
}

MyThread::~MyThread()
{
    if(_thrd)
    {
        terminate();
        _thrd->join();
//        _thrd->detach();
        delete _thrd;
    }
}

void MyThread::start()
{
    _thrd = new thread(_thread_func, this);
    
//    pthread_t thId = _thrd->native_handle();
//    
//    const sched_param prm = {99};
//    int err = pthread_setschedparam(thId, SCHED_RR, &prm);
//    if(err)
//        cerr << "Set prio error: " << strerror(err) << endl;
}

void MyThread::terminate()
{
    terminated = true;
}

void MyThread::_thread_func(MyThread* _this)
{
    _this->exec();
}