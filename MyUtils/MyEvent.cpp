/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyEvent.cpp
 * Author: user
 * 
 * Created on 8 апреля 2017 г., 11:07
 */

#include "MyEvent.h"

MyEvent::MyEvent() {
}

MyEvent::MyEvent(const MyEvent& orig) {
}

MyEvent::~MyEvent() {
}

void MyEvent::wait(int tm)
{
    std::unique_lock<std::mutex> locker(mut);
    
    if(tm == TM_INFINITE)
        cv.wait(locker);
    else
        cv.wait_for(locker, std::chrono::milliseconds(tm));
}

void MyEvent::notify(bool all)
{
    if(all)
        cv.notify_all();
    else
        cv.notify_one();
}

