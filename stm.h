#pragma once
#include "consts.h"

#include <wiringPi.h>
#include <wiringPiSPI.h>

#include <algorithm>
#include <array>
#include <chrono>
#include <cstdint>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <thread>
#include <unistd.h>

const char portname[] = "/dev/ttyAMA0";

struct RawData
{
    uint16_t matrix[consts::inputChannelCount*2];
    uint16_t failure;
    uint16_t audio[consts::blockSize];
};

class Stm{
	int dataAvaiblePin;
	int chipSelectPin;
	int spiChannel;
        int fd;
        int spi_fd;

	void *gpio_map;
	int mem_fd;
	volatile unsigned *gpio;
	bool sendFF = false;
	void setGpio(bool set);
        int set_interface_attribs (int fd, int speed, int parity);
        bool set_blocking (int fd, int should_block);
public:
	Stm(int chipSelectPin, int dataAvaiblePin, int spiChannel, int speed);

	bool dataAvailable();
	bool init();
	void getRawData(RawData &tempArray);
	void nextSpiPacketWithFF(bool value);
};
