/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StateSender.cpp
 * Author: user
 * 
 * Created on 4 мая 2017 г., 9:22
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>
#include <iostream>
#include <sys/time.h>

#include "StateSender.h"

using namespace std;

StateSender::StateSender(string ip, uint16_t port) : _ip(ip), _port(port) {
    sock = -1;
    if(ip == "" || port == 0)
        return;
    
    sock = socket(AF_INET, SOCK_DGRAM,0);
    if(sock < 0)
    {
        cerr << strerror(sock) << endl;
        return;
    }
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    int err = bind(sock, (sockaddr*)&addr, sizeof(sockaddr_in));
    if(err < 0){
        cerr << strerror(err) << endl;
        return;
    }
}

StateSender::StateSender(const StateSender& orig) {
}

StateSender::~StateSender() {
}

void StateSender::sendState(){
//    cout << "Send state" << endl;
    if(sock < 0)
        return;
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(_ip.c_str());
    addr.sin_port = htons(_port);
    
    int err = sendto(sock, (const void*)&rawData, sendSize, 0, (sockaddr*)&addr, sizeof(sockaddr_in));
    if(err < 0){
        cerr << strerror(err) << endl;
        return;
    }
}

//void StateSender::setFailure(uint8_t new_failure){
//    uint32_t tm = getTime();
//    if(failure != new_failure || (tm - tm_last_send) > 1000){
//        failure = new_failure;
//        tm_last_send = tm;
//        
//        sendState();
//    }
//}

void StateSender::setRawData(RawData &rawData){
    uint32_t tm = getTime();
    
    bool eq = (memcmp(&this->rawData, &rawData, sendSize) == 0);
    
    if(!eq || (tm - tm_last_send) > 1000){
        if(!eq)
            memcpy(&this->rawData, &rawData, sendSize);
        tm_last_send = tm;
        sendState();
    }
}

uint32_t StateSender::getTime(){
    timeval tv;
    gettimeofday(&tv,0);
    return tv.tv_sec*1000 + tv.tv_usec/1000;
}